//
//  UserDTO.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 16/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import Foundation
import Mapper

public struct UserDTO: Mappable {
    let email: String?
    let authToken: String?
    
    public init(map: Mapper) throws {
        email = map.optionalFrom("email")
        authToken = map.optionalFrom("authToken")
    }
}
