//
//  ProspectResponseModel.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 18/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import Foundation
import Mapper

public struct ProspectDTO: Mappable {
    let id: String?
    var name: String?
    var surname: String?
    var telephone: String?
    var schProspectIdentification: String?
    var address: String?
    var createdAt: String?
    var updatedAt: String?
    var statusCd: Int?
    var zoneCode: String?
    var neighborhoodCode: String?
    var cityCode: String?
    var sectionCode: String?
    var roleId: String?
    var appointableId: String?
    var rejectedObservation: String?
    var observation: String?
    var disable: String?
    var visited: String?
    var callcenter: String?
    var acceptSearch: String?
    var campaignCode: String?
    var userId: String?
    
    public init(map: Mapper) {
        id = map.optionalFrom("id")
        name = map.optionalFrom("name")
        surname = map.optionalFrom("surname")
        telephone = map.optionalFrom("telephone")
        schProspectIdentification = map.optionalFrom("schProspectIdentification")
        address = map.optionalFrom("address")
        createdAt = map.optionalFrom("createdAt")
        updatedAt = map.optionalFrom("updatedAt")
        statusCd = map.optionalFrom("statusCd")
        zoneCode = map.optionalFrom("zoneCode")
        neighborhoodCode = map.optionalFrom("neighborhoodCode")
        cityCode = map.optionalFrom("cityCode")
        sectionCode = map.optionalFrom("sectionCode")
        roleId = map.optionalFrom("roleId")
        appointableId = map.optionalFrom("appointableId")
        rejectedObservation = map.optionalFrom("rejectedObservation")
        observation = map.optionalFrom("observation")
        disable = map.optionalFrom("disable")
        visited = map.optionalFrom("visited")
        callcenter = map.optionalFrom("callcenter")
        acceptSearch = map.optionalFrom("acceptSearch")
        campaignCode = map.optionalFrom("campaignCode")
        userId = map.optionalFrom("userId")
    }
}
