//
//  LoginDomain.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 16/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import Foundation
import RxSwift

class RepositoryDomain {
    let testInterGrupoRepository: TestInterGrupoRepository
    
    init(testInterGrupoRepository: TestInterGrupoRepository) {
        self.testInterGrupoRepository = testInterGrupoRepository
    }
    
    func login(email: String, password: String) -> Observable<UserDTO?> {
        return testInterGrupoRepository.login(email: email, password: password)
    }
    
    func fetchProspects(token: String) -> Observable<[ProspectDTO]?> {
        return testInterGrupoRepository.fetchProspects(token: token)
    }
}
