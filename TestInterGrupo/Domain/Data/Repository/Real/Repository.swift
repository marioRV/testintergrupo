//
//  Repository.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 16/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import RxCocoa
import Moya_ModelMapper

final class Repository: TestInterGrupoRepository {
    
    let provider = MoyaProvider<InterGrupo>()
    
    func login(email: String, password: String) -> Observable<UserDTO?> {
        return provider.rx.request(.login(email, password)).debug().mapOptional(to: UserDTO.self).asObservable().share()
    }

    func fetchProspects(token: String) -> Observable<[ProspectDTO]?> {
        
        
//        provider.request(.fetchProspects(token)) { result in
//            switch result {
//            case let .success(response):
//                do {
//                    let repos = try response.map(to: [ProspectDTO].self) as [ProspectDTO]
////                    self.repos = repos
//                    print("z")
//                } catch {
//                    print("Unexpected error: \(error).")
//                }
////                self.tableView.reloadData()
//            case let .failure(error):
//                guard let error = error as? CustomStringConvertible else {
//                    break
//                }
////                self.showAlert("GitHub Fetch", message: error.description)
//            }
//        }
        
//        try response.mapArray() as [Repository]
        
        return provider.rx.request(.fetchProspects(token)).debug().mapOptional(to: [ProspectDTO].self).asObservable().share()
    }
}


