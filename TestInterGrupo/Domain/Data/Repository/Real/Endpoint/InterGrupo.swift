//
//  InterGrupo.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 17/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import Foundation
import Moya

//private extension String {
//    var URLEscapedString: String {
//        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
//    }
//}

enum InterGrupo {
    case login(String, String)
    case fetchProspects(String)
}

extension InterGrupo: TargetType {
    var baseURL: URL {
        return URL(string: "http://directotesting.igapps.co")!
    }
    
    var path: String {
        switch self {
        case .login(_,_):
            return "/application/login"
        case .fetchProspects(_):
            return "/sch/prospects.json"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Task {
        switch self {
        case .login(_,_):
            return .requestParameters(parameters: parameters!, encoding: URLEncoding.default)
        default:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .fetchProspects(let token):
            return ["token": token]
        case .login(_,_):
            return nil
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .login(let email, let password):
            var parameters = [String: Any]()
            parameters["email"] = email
            parameters["password"] = password
            return parameters
        default:
            return nil
        }
    }
    
    var sampleData: Data {
        switch self {
        case .login(_,_):
            return "{{\"id\": \"1\", \"language\": \"Swift\", \"url\": \"https://api.github.com/repos/mjacko/Router\"}}".data(using: .utf8)!
        case .fetchProspects(_):
            return "Half measures are as bad as nothing at all.".data(using: String.Encoding.utf8)!
        }
    }
}
