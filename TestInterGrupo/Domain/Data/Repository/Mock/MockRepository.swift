//
//  MockRepository.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 16/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import Foundation
import RxSwift

final class MockRepository: TestInterGrupoRepository {
    
//    func login(email: String, password: String, completion: @escaping (UserDTO?) -> ()) {
//        var success = ["success": "true", "authToken": "9FQn5gzKA9k2LWyBMZtM", "email": "directo@directo.com", "zone": "null"]
//
//        var fail = ["code": "134", "error": "Contraseña incorrecta"]
//    }
    
    func login(email: String, password: String) -> Observable<UserDTO?> {
//    func login(email: String, password: String) {
        return Observable.just(nil)
    }
    
    func fetchProspects(token: String) -> Observable<[ProspectDTO]?> {
        return Observable.just(nil)
    }
}
