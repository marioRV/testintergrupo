//
//  LoginRepository.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 16/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import Foundation
import RxSwift

protocol TestInterGrupoRepository {
    func login(email: String, password: String) -> Observable<UserDTO?>
    func fetchProspects(token: String) -> Observable<[ProspectDTO]?>
}
