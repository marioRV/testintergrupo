//
//  TestDependencyInjection.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 16/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import Foundation
import Swinject

class TestDependencyInjection {
    static func registerTypes(container: Container) {
        
        if (ProcessInfo.processInfo.environment["MOCKS"] != nil) {
            container.register(TestInterGrupoRepository.self) {_ in MockRepository()}//.inObjectScope(.container)
        }
        else {
            container.register(TestInterGrupoRepository.self) {_ in Repository()}//.inObjectScope(.container)
        }
        
        container.register(RepositoryDomain.self) {r in
            RepositoryDomain(testInterGrupoRepository: r.resolve(TestInterGrupoRepository.self)!)
        }
    }
}
