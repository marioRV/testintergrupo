//
//  LoginViewController.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 15/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol LoginViewControllerDelegate: class {
    func didLogin()
}

class LoginViewController: UIViewController {
    
    @IBOutlet weak var autologinSwich: UISwitch!
    @IBOutlet weak var autoLoginLabel: UILabel!
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let disposeBag = DisposeBag()
    weak var delegate: LoginViewControllerDelegate?
    
    let viewModel: LoginViewModelType
    
    init(viewModel: LoginViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userTextField.placeholder = NSLocalizedString("User", comment: "user placeholder")
        passwordTextField.placeholder = NSLocalizedString("Password", comment: "user placeholder")
        loginButton.setTitle(NSLocalizedString("Login", comment: "Login title"), for: .normal)
        autoLoginLabel.text = NSLocalizedString("Auto login?", comment: "auto login text")
        
//        userTextField.text = "directo@directo.com"
//        passwordTextField.text = "directo123"
        
        activityIndicator.isHidden = true
        
        setupRx()
    }
    
    func setupRx() {
        _ = userTextField.rx.text.map{ $0 ?? "" }.bind(to: viewModel.inputs.email)
        _ = passwordTextField.rx.text.map{ $0 ?? "" }.bind(to: viewModel.inputs.password)
        _ = viewModel.outputs.isButtonEnabled.bind(to: loginButton.rx.isEnabled)
        
        loginButton.rx.tap.subscribe(onNext: { [unowned self] in
            
            if self.userTextField.isFirstResponder == true || self.passwordTextField.isFirstResponder == true {
                self.view.endEditing(true)
            }
            self.autologinSwich.isEnabled = false
            self.loginButton.isEnabled = false
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()

            self.viewModel.inputs.loginAction(email: self.userTextField.text!, password: self.passwordTextField.text!, store: self.autologinSwich.isOn).subscribe(onNext: { [unowned self] (result) in

                self.loginButton.isEnabled = true
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()

                if !result {
                    self.showErrorAlertView()
                    self.autologinSwich.isEnabled = true
                } else {
                    self.delegate?.didLogin()
                }
//            self.subscribeLogin(email: self.userTextField.text!, password: self.passwordTextField.text!)
            }).disposed(by: self.disposeBag)
        }).disposed(by: disposeBag)
    }
        
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if AppParams.userIsLogged() {
            
            userTextField.text = viewModel.outputs.getStoredCredentials()[0]
            passwordTextField.text = viewModel.outputs.getStoredCredentials()[1]
            
            autologinSwich.isEnabled = false
            loginButton.isEnabled = false
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            
            viewModel.inputs.autologin().subscribe(onNext: { [unowned self] (result) in
            
                self.loginButton.isEnabled = true
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
                if !result {
                    self.showErrorAlertView()
                    self.autologinSwich.isEnabled = true
                } else {
                    self.delegate?.didLogin()
                }
            }).disposed(by: disposeBag)
        }
    }
    
    func showErrorAlertView() {
        let alertController = UIAlertController(title: NSLocalizedString("Oops!", comment: "title"), message:
            NSLocalizedString("Something went wrong, please try again", comment: "subject"), preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Accept", comment: "button title"), style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
