// LoginViewModel.swift
// TestInterGrupo
//
// Created by MacBook Air on 15/06/18.
//Copyright © 2018 mariorv. All rights reserved.
//

import Foundation
import RxSwift
//import RxCocoa
import Mapper
import SwiftHash
import KeychainAccess

public protocol LoginViewModelInputs {
    var email: Variable<String> { get set }
    var password: Variable<String> { get set }
    func loginAction(email: String, password: String, store: Bool) -> Observable<Bool>
    func autologin() -> Observable<Bool>
}

public protocol LoginViewModelOutputs {
    var isValidEmail: Observable<Bool> { get }
    var isValidPassword: Observable<Bool> { get }
    var isButtonEnabled: Observable<Bool> { get }
    func getStoredCredentials() -> [String?]
}

public protocol LoginViewModelType {
    var inputs: LoginViewModelInputs { get }
    var outputs: LoginViewModelOutputs { get }
}

public final class LoginViewModel: LoginViewModelType, LoginViewModelInputs, LoginViewModelOutputs {
    
    public var email: Variable<String>
    public var password: Variable<String>
    
    public var inputs: LoginViewModelInputs { return self }
    public var outputs: LoginViewModelOutputs { return self }
    
    private let domain: RepositoryDomain
    private let keychain = Keychain(service: Bundle.main.bundleIdentifier!)
    
//    public init() {
    init(domain: RepositoryDomain) {
        email = Variable<String>("")
        password = Variable<String>("")
//        domain = AppDelegate.diContainer.resolve(RepositoryDomain.self)!
        self.domain = domain
    }

    public var isValidEmail: Observable<Bool> {
        return email.asObservable().map({ [weak self] (String) -> Bool in
            guard let result = self?.checkEmail(email: (self?.email.value)!) else { return false }
            return result
        })
    }
    
    public var isValidPassword: Observable<Bool> {
        return password.asObservable().map({ [weak self] (String) -> Bool in
            guard let result = self?.checkPassword(password: self?.password.value) else { return false }
            return result
        })
    }
    
    public var isButtonEnabled: Observable<Bool> {
        return Observable.combineLatest(isValidEmail, isValidPassword) { $0 && $1 }
    }
    
    func checkEmail(email:String?) -> Bool {
        guard email != nil else { return false }
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: email)
    }
    
    func checkPassword(password: String?) -> Bool {
        guard password != nil else { return false }
        return password!.count > 4
    }
    
    public func loginAction(email: String, password: String, store: Bool) -> Observable<Bool> {
        return domain.login(email: email, password: password).flatMap{ userDTO -> Observable<Bool> in
            guard userDTO?.authToken != nil else { return Observable.just(false) }
            
            self.keychain["token"] = /*MD5(userDTO!.authToken!)*/ userDTO!.authToken!
            
            if(store)
            {
                self.keychain["email"] = /*MD5(email)*/ email
                self.keychain["password"] = /*MD5(password)*/ password
                AppParams.setUserIsLogged(isLogged: true)
            }
            return Observable.just(true)
        }
    }
    
    public func getStoredCredentials() -> [String?] {
        return [keychain["email"], keychain["password"]]
    }
    
    public func autologin() -> Observable<Bool> {
        let storedEmail = keychain["email"]
        let storedPassword = keychain["password"]
        
        guard storedEmail != nil else { return Observable.just(false) }
        guard storedPassword != nil else { return Observable.just(false) }
        
        return loginAction(email: storedEmail!, password: storedPassword!, store: false)
    }
}
