// ProspectsViewModel.swift
// TestInterGrupo
//
// Created by MacBook Air on 18/06/18.
//Copyright © 2018 mariorv. All rights reserved.
//

import Foundation
import RxSwift
import KeychainAccess

public protocol ProspectsViewModelInputs {
    func fetchProspects() -> Observable<[ProspectDTO]?>
    func closeSession()
}

public protocol ProspectsViewModelOutputs {
    
}

public protocol ProspectsViewModelType {
    var inputs: ProspectsViewModelInputs { get }
    var outputs: ProspectsViewModelOutputs { get }
}


public final class ProspectsViewModel: ProspectsViewModelType, ProspectsViewModelInputs, ProspectsViewModelOutputs {
    
    public var inputs: ProspectsViewModelInputs { return self }
    public var outputs: ProspectsViewModelOutputs { return self }
    
    private let domain: RepositoryDomain
    private let keychain = Keychain(service: Bundle.main.bundleIdentifier!)
    
    init(domain: RepositoryDomain) {
        self.domain = domain
    }
    
    public func fetchProspects() -> Observable<[ProspectDTO]?> {
        return domain.fetchProspects(token: keychain["token"]!)
    }
    
    public func closeSession() {
        keychain["token"] = nil
        keychain["email"] = nil
        keychain["password"] = nil
    }
}
