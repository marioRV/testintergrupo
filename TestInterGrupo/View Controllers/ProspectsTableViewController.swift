//
//  ProspectsTableViewController.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 18/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

protocol ProspectsTableViewControllerDelegate: class {
    func didTapCloseSession()
}

class ProspectsTableViewController: UITableViewController {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    weak var delegate: ProspectsTableViewControllerDelegate?
    
    let viewModel: ProspectsViewModelType
    var prospects: [ProspectDTO]?
    let disposeBag = DisposeBag()
    
    init(viewModel: ProspectsViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        menuButton.target = self.revealViewController()
//        menuButton.action = #selector(SWRevealViewController.revealToggle(_:))

        navigationItem.leftBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Close session", comment: "close session title"), style: .plain, target: self, action: #selector(closeTapped))

        
        let nib = UINib(nibName: "Cell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "prospectCell")
        setupRx()
    }
    
    func setupRx() {
//        viewModel.inputs.fetchProspects().bind(to: tableView!.rx.items(cellIdentifier: "cell")) { row, element, cell in
//            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: IndexPath(row: row, section: 0))
//            cell.textLabel?.text = item.title
//
//            return cell
//        }
        

        viewModel.inputs.fetchProspects().subscribe(onNext: { [unowned self] (prosp) in
            self.prospects = prosp
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
    }
    
    @objc
    func closeTapped() {
        viewModel.inputs.closeSession()
        delegate?.didTapCloseSession()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 131
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        guard prospects != nil else { return 0 }
        return prospects!.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "prospectCell", for: indexPath) as! TableViewCell

        var completeName = prospects![indexPath.row].name
        completeName?.append(" ")
        completeName?.append(prospects![indexPath.row].surname!)
        cell.nameLabel?.text = completeName
        cell.ccLabel.text = prospects![indexPath.row].schProspectIdentification
        cell.telLabel.text = prospects![indexPath.row].telephone
        
        let status = prospects![indexPath.row].statusCd
        cell.statusImageView.image = UIImage(named: getImagenameFromStatus(status: status))
        return cell
    }
    
    func getImagenameFromStatus(status: Int?) -> String {
        switch status {
        case 0:
            return "pending"
        case 1:
            return "approved"
        case 2:
            return "accepted"
        case 3:
            return "rejected"
        case 4:
            return "disabled"
        default:
            return "noname"
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
