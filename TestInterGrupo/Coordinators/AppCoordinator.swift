//
//  AppCoordinator.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 15/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import UIKit
import Foundation

final class AppCoordinator: Coordinator {
    
    let navigationController: UINavigationController
    let domain: RepositoryDomain
//    var childCoordinators : [Coordinator] = []
    
    init(navigationController: UINavigationController, domain: RepositoryDomain) {
        self.navigationController = navigationController
        self.domain = domain
    }
    
    func start() {
        showLogin()
    }
    
    func showLogin() {
        let coordinator = LoginCoordinator(navigationController: navigationController)
        coordinator.delegate = self
        coordinator.start(domain: domain)
//        childCoordinators.append(coordinator)
        add(child: coordinator)
    }
    
    func showDashboard() {
        let coordinator = ProspectsCoordinator(navigationController: navigationController)
        coordinator.delegate = self
        coordinator.start(domain: domain)
        //        childCoordinators.append(coordinator)
        add(child: coordinator)
    }
}

extension AppCoordinator: LoginCoordinatorDelegate {
    func finish(coordinator: Coordinator) {
        remove(child: coordinator)
        showDashboard()
    }
}

extension AppCoordinator: ProspectsCoordinatorDelegate {
    func closeSession(coordinator: Coordinator) {
        remove(child: coordinator)
        AppParams.setUserIsLogged(isLogged: false)
        showLogin()
    }
}

