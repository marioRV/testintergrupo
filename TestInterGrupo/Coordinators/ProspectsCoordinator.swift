//
//  ProspectsCoordinator.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 15/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import UIKit
import Foundation

protocol ProspectsCoordinatorDelegate: class {
    func closeSession(coordinator: Coordinator)
}

final class ProspectsCoordinator: Coordinator {
    
    let navigationController: UINavigationController
    let storyboard: UIStoryboard
    weak var delegate: ProspectsCoordinatorDelegate?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.storyboard = UIStoryboard(name: "Main", bundle: nil)
    }
    
    func start(domain: RepositoryDomain) {
        let viewModel = ProspectsViewModel(domain: domain)
//        let viewController = ProspectsViewController(viewModel: viewModel)
        let viewController = ProspectsTableViewController(viewModel: viewModel)
        viewController.delegate = self
//        let viewController = storyboard.instantiateViewController(withIdentifier: "prospects")
        navigationController.viewControllers = [viewController]
    }
}

extension ProspectsCoordinator: ProspectsTableViewControllerDelegate {
    func didTapCloseSession() {
        delegate?.closeSession(coordinator: self)
    }
}
