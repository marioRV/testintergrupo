//
//  LoginCoordinator.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 15/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import UIKit
import Foundation

protocol LoginCoordinatorDelegate: class {
    func finish(coordinator: Coordinator)
}

final class LoginCoordinator: Coordinator {

    weak var delegate: LoginCoordinatorDelegate?
    let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start(domain: RepositoryDomain) {
        let loginViewModel = LoginViewModel(domain: domain)
        let viewController = LoginViewController(viewModel: loginViewModel)
        viewController.delegate = self
//        navigationController.pushViewController(viewController, animated: true)
        navigationController.viewControllers = [viewController]
    }
}

extension LoginCoordinator: LoginViewControllerDelegate {
    func didLogin() {
        delegate?.finish(coordinator: self)
    }
}
