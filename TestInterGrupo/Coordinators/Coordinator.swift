//
//  Coordinator.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 15/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import Foundation

//protocol Coordinator {
//    func start()
//}

class Coordinator {
    private var children: [Coordinator] = []
    
    func add(child: Coordinator) {
        guard !children.contains(where: { $0 === child }) else {
            return
        }
        
        children.append(child)
    }
    
    func remove(child: Coordinator) {
        guard let index = children.index(where: { $0 === child }) else {
            return
        }
        
        children.remove(at: index)
    }
    
    func removeAll() {
        children.removeAll()
    }
}
