//
//  AppParams.swift
//  TestInterGrupo
//
//  Created by MacBook Air on 15/06/18.
//  Copyright © 2018 mariorv. All rights reserved.
//

import Foundation
final class AppParams: NSObject
{
    enum Constants
    {   
        enum State: String {
            case userLogged
        }
    }
    
    private override init() { }
    
    class func setUserIsLogged(isLogged : Bool) -> Void {
        UserDefaults.standard.set(isLogged, forKey: Constants.State.userLogged.rawValue)
    }
    
    class func userIsLogged() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.State.userLogged.rawValue)
    }
}
